#!/usr/bin/env bash

function set_param {
    sed -i "s/$1: ~/$1: $2/" "${BASE_DIRECTORY}/${DIRECTORY}.data"/app/config/parameters.yml
}

cd ${BASE_DIRECTORY}
cp ${DIRECTORY}.data/app/config/parameters.yml.dist ${DIRECTORY}.data/app/config/parameters.yml

set_param "database_host" ${DATABASE_HOST}
set_param "database_port" ${DATABASE_PORT}
set_param "database_name" ${DATABASE_NAME}
set_param "database_name_test" "${DATABASE_NAME}_test"
set_param "database_user" ${DATABASE_USER}
set_param "database_password" ${DATABASE_PASSWORD}
set_param "protocol" ${PROTOCOL}
set_param "site" ${SITE}

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '93b54496392c062774670ac18b134c3b3a95e5a5e5c8f1a9f115f203b75bf9a129d5daa8ba6a13e2cc8a1da0806388a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"

./composer.phar install --no-interaction --working-dir ./${DIRECTORY}.data
${DIRECTORY}.data/bin/console ckeditor:install
${DIRECTORY}.data/bin/console assets:install --symlink
${DIRECTORY}.data/bin/console c:c -e prod
ln -s ${BASE_DIRECTORY}/${DIRECTORY}.data/web ${BASE_DIRECTORY}/${DIRECTORY}
