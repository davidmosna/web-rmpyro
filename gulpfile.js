'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');

const paths = {
    site: {
        sass: [
            'src/UIBundle/Resources/src/sass/*'
        ],
    },
    fa: {
        js: [
            'node_modules/@fortawesome/fontawesome-free/js/*'
        ],
        css: [
            'node_modules/@fortawesome/fontawesome-free/css/*'
        ],
        img: [
            'node_modules/@fortawesome/fontawesome-free/sprites/*'
        ],
        fonts: [
            'node_modules/@fortawesome/fontawesome-free/webfonts/*'
        ]
    }
};

const options = {
    autoprefixer: {
        browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
    },
    sass: {
        errLogToConsole: true,
        outputStyle: 'compressed'
    },
};

gulp.task('site-sass', function () {
    return gulp.src(paths.site.sass)
        .pipe(sourcemaps.init())
        .pipe(sass(options.sass).on('error', sass.logError))
        .pipe(autoprefixer(options.autoprefixer))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('web/css'));
});

gulp.task('fa-js', function () {
   return gulp.src(paths.fa.js)
       .pipe(gulp.dest('web/js'));
});

gulp.task('fa-fonts', function(){
    return gulp.src(paths.fa.fonts)
        .pipe(gulp.dest('web/webfonts'));
});

gulp.task('default', ['build']);
gulp.task('build', ['site-sass', 'fa-fonts', 'fa-js']);

