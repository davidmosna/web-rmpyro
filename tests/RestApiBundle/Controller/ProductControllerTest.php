<?php

namespace RestApiBundle\Tests\Controller;

use RestApiBundle\Tests\ApiTestAssertAwareTrait;
use RestApiBundle\Tests\ApplicationTestAwareTrait;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\StringInput;

class ProductControllerTest extends WebTestCase
{

    use ApiTestAssertAwareTrait;
    use ApplicationTestAwareTrait;

    public function testGetProductsByAvailability(){}
    public function testGetProductsByAvailabilityWithLimit(){}
    public function testGetProductsByAvailabilityWithLimitAndOffset(){}
    public function testGetProductSuccess(){}
    public function testGetProductFailure(){}
    public function testPostProduct(){}
    public function testPatchProductSetDeleted(){}
    public function testPatchProductSetDoubleDeleted(){}

}
