<?php

namespace UIBundle\Controller;

use RestApiBundle\Entity\Product;
use RestApiBundle\Utils\Helpers;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ProductController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $products = $this->getDoctrine()->getRepository(Product::class)->findVisibleOrderedByNewDESCAndPriceASC();

        return $this->render('@UI/index.html.twig', ['products' => $products]);
    }

    /**
     * @Route("/{slug}", name="product_detail")
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\Response|\Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function productDetailAction(Product $product)
    {
        if (!$product->getIsVisible()) {
            return $this->createAccessDeniedException();
        }

        $product->setVideo(Helpers::createEmbedUrl($product->getVideo()));
        return $this->render('@UI/product-detail.twig', ['product' => $product]);
    }
}
