<?php

namespace RestApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use RestApiBundle\Entity\Product;
use RestApiBundle\Repository\ProductRepository;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ProductController
 * @package RestApiBundle\Controller
 */
class APIProductController extends FOSRestController
{

    /**
     * @Rest\Get("/products")
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Return a collection of Products"
     * )
     * @return \FOS\RestBundle\View\View
     */
    public function cgetProductsAction()
    {

        $products = $this->getProductRepository()->findAll();
        return $this->view($products);
    }


    /**
     * @Rest\Get("/products/{id}")
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Return a Product",
     *  requirements={
     *     {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the Product"
     *      }
     *  }
     * )
     * @param $id
     * @return \FOS\RestBundle\View\View|NotFoundHttpException
     */
    public function getProductByIdAction($id)
    {
        /** @var Product $product */
        $product = $this->getProductRepository()->find($id);

        if (null == $product) {
            return $this->createNotFoundException();
        }

        return $this->view($product);
    }


    /**
     * @Rest\Get("/products/by-slug/{slug}")
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Return a Product",
     *  requirements={
     *     {
     *          "name"="slug",
     *          "dataType"="string",
     *          "requirement"=".+",
     *          "description"="slug of the Product"
     *      }
     *  }
     * )
     * @param $slug
     * @return \FOS\RestBundle\View\View|NotFoundHttpException
     */
    public function getProductBySlugAction($slug)
    {
        $product = $this->getProductRepository()->findOneBy([
            'slug' => $slug
        ]);

        if (null == $product) {
            return $this->createNotFoundException();
        }

        return $this->view($product);
    }


    /**
     * @Rest\Get("/products/by-code/{barcode}")
     *
     * @ApiDoc(
     *  resource=true,
     *  description="Return a Product",
     *  requirements={
     *     {
     *          "name"="code",
     *          "dataType"="string",
     *          "requirement"=".+",
     *          "description"="barcode of the Product"
     *      }
     *  }
     * )
     * @param $barcode
     * @return \FOS\RestBundle\View\View|NotFoundHttpException
     */
    public function getProductByCodeAction($barcode)
    {
        $product = $this->getProductRepository()->findOneBy([
            'barcode' => $barcode
        ]);

        if (null == $product) {
            return $this->createNotFoundException();
        }

        return $this->view($product);
    }


    /**
     * @return object|ProductRepository
     */
    protected function getProductRepository()
    {
        return $this->get('restapi.entity.repository.product');
    }
}
