<?php
/**
 * Created by PhpStorm.
 * User: dave
 * Date: 04/08/2018
 * Time: 03:44
 */

namespace RestApiBundle\Utils;


class Helpers
{

    /**
     * @param $url
     * @return mixed|string
     */
    public static function createEmbedUrl($url)
    {
        if ($url) {
            $url = parse_url($url);
            parse_str($url['query'], $query);
            return 'https://www.youtube.com/embed/' . $query['v'];
        }

        return $url;
    }
}