<?php

namespace RestApiBundle\Command;

use Cocur\Slugify\Slugify;
use RestApiBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class UpdateProductAttributesCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('app:update-products')
            ->setDescription('Update product attributes such as slug, images,...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $products = $em->getRepository(Product::class)->findAll();
        /** @var Product $product */
        foreach ($products as $product) {
            $slug = (new Slugify())->slugify($product->getName());
            $product->setSlug($slug);
            $product->setImage($slug . '.jpg');
        }

        $em->flush();
        $output->writeln('Slugs updated!');
    }
}