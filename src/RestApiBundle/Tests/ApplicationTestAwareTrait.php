<?php

namespace RestApiBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\Event\ConsoleErrorEvent;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Response;

trait ApplicationTestAwareTrait
{
    /**
     * @var Application
     */
    protected static $application;

    /**
     * @var Client
     */
    protected static $client;

    /**
     * @var EventDispatcher
     */
    protected static $dispatcher;

    /**
     * runs before first test case
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        self::$client = static::createClient();

        self::runCommand('doctrine:database:create --env=test');
    }

    /**
     * runs after last tescase
     */
    public static function tearDownAfterClass()
    {
        parent::tearDownAfterClass();

        self::runCommand('doctrine:database:drop --force --env=test');
    }

    /**
     * runs before each testcase
     */
    public function setUp()
    {
        self::createSchema();
    }

    /**
     * runs after each testcase
     */
    public function tearDown()
    {
        self::dropSchema();
    }

    /**
     * creates database schema
     */
    public static function createSchema()
    {
        self::runCommand('doctrine:schema:create --env=test');
    }

    /**
     * drops database schema
     */
    public static function dropSchema()
    {
        self::runCommand('doctrine:schema:drop --force --env=test');
    }

    /**
     * @param $command
     * @return mixed
     */
    private static function runCommand($command)
    {
        $command = sprintf('%s --quiet', $command);
        return self::getApplication()->run(new StringInput($command), new NullOutput());
    }

    /**
     * @return Application
     */
    private static function getApplication()
    {
        if (null === self::$application) {
            self::$application = new Application(self::$client->getKernel());
            self::$dispatcher = new EventDispatcher();
            self::$dispatcher->addListener(ConsoleEvents::ERROR, function(ConsoleErrorEvent $event){});
            self::$application->setDispatcher(self::$dispatcher);
            self::$application->setAutoExit(false);
        }

        return self::$application;
    }

    /**
     * @param string $url
     * @param array $data
     * @return Response|null
     */
    protected function sendRequest($method, $url, array $data = [])
    {
        self::$client->request($method, $url, $data);

        return self::$client->getResponse();
    }

    /**
     * load fixtures data (need to be set-up in configuration)
     *
     * @param array $tags array of tags to load
     */
    protected static function loadFixtures(array $tags = ['test'])
    {
        self::$client->getContainer()->get('khepin.yaml_loader')->loadFixtures(implode(",",$tags));
    }

    /**
     * @param string $json
     * @param array $keysToFilter
     * @return string
     */
    protected function filterJsonData(string $json, array $keysToFilter)
    {
        $cb = function($k) use ($keysToFilter) {
            if (in_array($k, $keysToFilter, true)) {
                return false;
            }
            return true;
        };

        $decoded = json_decode($json, true);
        $filtered = $this->array_filter_recursive($decoded, $cb);

        return json_encode($filtered);
    }

    /**
     * @param array $input
     * @param null $callback
     * @return array
     */
    protected function array_filter_recursive(array $input, $callback = null)
    {
        foreach ($input as &$value) {
            if (is_array($value)) {
                $value = $this->array_filter_recursive($value, $callback);
            }
        }

        return array_filter($input, $callback, ARRAY_FILTER_USE_KEY);
    }
}