<?php

namespace RestApiBundle\Tests;

use Symfony\Component\HttpFoundation\Response;

trait ApiTestAssertAwareTrait
{
    /**
     * @param Response $response
     * @param int $statusCode
     */
    protected function assertJsonResponse($response, $statusCode = Response::HTTP_OK)
    {
        $this->assertEquals(
            $statusCode,
            $response->getStatusCode(),
            $response->getContent()
        );
        if ($statusCode !== Response::HTTP_NO_CONTENT) {
            $this->assertTrue(
                $response->headers->contains('Content-Type', 'application/json'),
                $response->headers
            );
        }
    }
}
