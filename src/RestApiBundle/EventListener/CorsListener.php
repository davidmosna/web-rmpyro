<?php

namespace RestApiBundle\EventListener;

use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


/**
 * Class CorsListener
 *
 * @package api-database
 * @subpackage ShiftPlan\ToolboxBundle\EventListener
 *
 * @copyright Impulsfaktor GmbH
 * @author Martin Holoubek <martin.holoubek-ext@xitee.com>
 */
class CorsListener implements EventSubscriberInterface
{
    /**
     * Subscribed Events
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::REQUEST  => array('onKernelRequest', 9999),
            KernelEvents::RESPONSE => array('onKernelResponse', 9999),
        );
    }

    /**
     * Modify requests - add OPTIONS
     *
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (HttpKernelInterface::MASTER_REQUEST !== $event->getRequestType()) {
            return;
        }

        if ('OPTIONS' == $event->getRequest()->getRealMethod()) {
            $response = new Response();
            $event->setResponse($response);
        }
    }

    /**
     * Modify responses - add CORS headers
     *
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        if (HttpKernelInterface::MASTER_REQUEST !== $event->getRequestType()) {
            return;
        }

        $response = $event->getResponse();

        //CORS response headers
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'GET,POST,PUT,PATCH,DELETE');
        $response->headers->set('Access-Control-Allow-Headers', 'Accept,Content-Type,Authorization,X-Switch-User');
        $response->headers->set('Access-Control-Expose-Headers', 'X-Total-Count,Location,Content-Disposition,Content-Type,Content-Length,Cache-Control');
    }
}