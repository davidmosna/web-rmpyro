<?php

namespace AdminBundle\Controller;

use JavierEguiluz\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;

class AdminController extends BaseAdminController
{

    /**
     * @param object $entity
     */
    public function prePersistEntity($entity)
    {
    }

    /**
     * @param $entity
     */
    public function update($entity)
    {
    }

    /**
     * @param object $entity
     */
    public function preUpdateEntity($entity)
    {
    }
}